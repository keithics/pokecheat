var express = require('express');
var fs = require('fs');
var exec = require('child_process').exec;
var app = express();
var rest = require('restler');
var bodyParser = require('body-parser');
var cloudscraper = require('cloudscraper');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(express.static('public'));


//clicker
var firstClick = function(){
  exec('osascript ./autoclick.scpt', function(err, stdout, stderr) {
    console.log('clicked');
  });

}


var vision = function(lat,lng,cb){
  //var site = 'https://pokevision.com/map/data/'+lat+'/'+lng
  var site = 'https://melpogomap.com/query2.php?since=1517072906&mons=6,8,9,26,59,67,68,76,83,89,94,106,107,112,113,122,124,125,130,131,134,135,136,143,145,146,147,148,149,150,151,154,157,158,160,176,181,196,197,201,212,214,233,237,242,243,244,245,247,248,249,250,251,254,257,260,282,289,292,304,305,306,319,330,336,348,350,365,369,372,373,376,382';
 cloudscraper.get(site, function(error, response, body) {
  if (error) {
    cb('Error occurred');
  } else {
    cb(null,body);
  }
});

}
app.post('/', function (req, res) {
  var xmlString = '<gpx creator="Xcode" version="1.1"><wpt lat="' + req.body.lat + '" lon="' + req.body.lng + '"><name>PokemonLocation</name></wpt></gpx>';
  fs.writeFile("./pokemonLocation.gpx", xmlString, function (err) {
    if (err) {

      res.send('err');
    } else {
      console.log('success --> lat: ' + req.body.lat + ' lng:' + req.body.lng);
      firstClick();
      res.send('ok');

    }


  });
});

app.post('/pokemons', function (req, res) {
  var lat = parseFloat(req.body.lat);
  var lng = parseFloat(req.body.lng);
      vision(lat,lng,function(err,data){
          if(err){
              res.send({});
          }else{
            res.send(data);
          }
      })
});

app.post('/run', function (req, res) {
  exec('./autoClicker -x 188 -y 45 ', function(err, stdout, stderr) {
    setTimeout(function(){
        exec('./autoClicker -x 135 -y 38 ', function(err, stdout, stderr) {
          setTimeout(function(){
            var secondClick = exec('./autoClicker -x 171 -y 66 ', function(err, stdout, stderr) {  
            console.log('restarting..');      
            });

          },250);

            res.send('ok');
        });
      },250)
})
});


app.listen(3000, function () {
  console.log('Pokemon GO Controller on port 3000!');
});

